# /e/ wiki

## FAQ

## Is /e/ open source? 

Yes - all source code is available and you can compile it, fork it... Some prebuilt applications are used in the system; they are built separately from source code available here, or synced from open source repositories such as F-Droid. We ship one proprietary application though ([read the statement](/maps)).

## Is /e/ LineageOS + microG?

/e/ is forked from LineageOS. We've modified several parts of the system (and we're just beginning): installation procedure, settings organization, default settings. We've disabled and/or removed any software or services that were sending personal data to Google (for instance, the default search engine is no longer Google). We've integrated microG by default, have replaced some of the default applications, and modified others. We have added a synchronization background software service that syncs multimedia contents (pictures, videos, audio, files...) and settings to a cloud drive, when activated. 

Also, we've replaced the LineageOS launcher with our own new launcher, written from scratch, that has a totally different look and feel from default LineageOS. 

We've implemented several /e/ online services, with a single /e/ user identity (user@e.email). This infrastructure will be offered as docker images for self hosting: drive, email, calendar... to those who prefer self-hosting.

We have added an account manager within the system with support for the single identity. It allows users to log only once, with a simple "user@e.email" identity, for getting access to /e/'s various online services (drive, email, calendar, notes, tasks).

## Why should I use /e/? I can customize the ROM myself...

Absolutely! /e/ was not made for geeks or power users, it's designed to offer a credible and attractive alternative to the average user, with more freedom and a better respect of user's data privacy compared to mobile operating systems offered by the worldwide duopoly in place.

We really appreciate your involvement though. Your beta-testing and valuable feedback will help to make this alternative available to many more people who can't do this sort of thing for themselves.    

## I'm a geek and I love the default AOSP/LineageOS look

In that case, you probably won't be very happy with /e/, don't use it!

## Aren't you stealing the work of LineageOS developers?

No - we're using the rules of open source software. Just like AOSP-Android is forking the Linux kernel work, just like LineageOS is forking AOSP work, /e/ is forking LineageOS work. /e/s focus is on the final end-user experience, and less on the hardware. We encourage core developers to contribute upstream to LineageOS. When thinking about LineageOS vs /e/, think about Debian vs Ubuntu.

## Does /e/ have a simple way for updating the system regularly?

Yes - a simple 1-click (well, that may be 2 or 3!) "over the air" update feature is available in settings.

## Is my XYZ123 device supported? 

Look at the [device list](/devices-list/). We are adding new devices regularly.

## From which version of Android is /e/ forked?

Android 7 (Nougat)/LOS14 (/e/-0.1 branch). We're working on porting our changes to Android 8 (Oreo)/LOS 15. You can find current changes for this port in "/e/-0.2" branches.

## Is /e/ stable?

No - we're in beta stage at the moment. The system is pretty usable though, but use it at your own risk!

## Will you offer /e/-preloaded phones?

Yes - that's the ultimate plan as soon as we find good hardware-partner(s).

## If I find a bug, should I send you an email?

No - please report bugs and send suggestions using this platform as [explained here](/issues/). Please avoid using email or IM for reporting issues, it's not an efficient process.

## If I have suggestion, how should I contact you?

You can also use this platform to report suggestions. If you have suggestions related to improve privacy, you can also send an email to privacychallenge@e.email

## Why this weird, inconvenient /e/ name?

We had to change our name for legal reasons [as explained here](https://www.indidea.org/gael/blog/leaving-apple-and-google-e-is-the-symbol-for-my-data-is-my-data/).

/e/ is a textual representation of our "e" symbol which means "my data is MY data".

It's the current project codename, we will probably introduce a new and more convenient name for our mobile ROM in a few months. 