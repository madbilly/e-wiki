# /e/ wiki

## Build /e/

>>>
ATTENTION : This process will only work on **case-sensitive** filesystems!
* Windows: will not work
* macOS: doesn't work either for HPS+ filesystem - adfs not tried
* Linux: work on Ubuntu and CentOS
>>>

## 0. System requirements
Your computer needs to be 64-bit and running a 64-bit operating system with at least 250GB of spare hard drive space and 8GB of RAM (or 16GB in a virtual machine).

## 1. Install docker

If you have not already done so, [install docker](https://docs.docker.com/install/).

For example, in Ubuntu 18.04 run
```
sudo apt install docker.io
```
NOTE: These build instructions do not work with the Snap installer for Docker (other steps may be required).

## 2. Get our docker image

```shell
$ sudo docker pull registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:latest
```

## 3. Create directories

```shell
$ sudo mkdir -p \
/e/src \
/e/zips \
/e/logs \
/e/ccache \
```

## 4. Start build

Run the following command. Don't forget to replace `<my-device>` with your device code !


```shell
$ sudo docker run \
-v "/e/src:/srv/src:delegated" \
-v "/e/zips:/srv/zips:delegated" \
-v "/e/logs:/srv/logs:delegated" \
-v "/e/ccache:/srv/ccache:delegated" \
-e "BRANCH_NAME=eelo-0.1" \
-e "DEVICE_LIST=<my-device>" \
-e "CUSTOM_PACKAGES='GmsCore GsfProxy FakeStore Telegram Signal Mail BlissLauncher BlissIconPack MozillaNlpBackend YahooWeatherProvider AccountManager MagicEarth MuPDF OpenCamera eDrive Weather Notes Tasks NominatimNlpBackend Light'"  \
-e "SIGNATURE_SPOOFING=restricted" \
-e "OTA_URL=https://ota.ecloud.global/api" \
-e "REPO=https://gitlab.e.foundation/e/os/android.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:latest
```

>>>
Example for kiwi:
```shell
$ sudo docker run \
-v "/e/src:/srv/src:delegated" \
-v "/e/zips:/srv/zips:delegated" \
-v "/e/logs:/srv/logs:delegated" \
-v "/e/ccache:/srv/ccache:delegated" \
-e "BRANCH_NAME=eelo-0.1" \
-e "DEVICE_LIST=kiwi" \
-e "CUSTOM_PACKAGES='GmsCore GsfProxy FakeStore Telegram Signal Mail BlissLauncher BlissIconPack MozillaNlpBackend YahooWeatherProvider AccountManager MagicEarth MuPDF OpenCamera eDrive Weather Notes Tasks NominatimNlpBackend Light'"  \
-e "SIGNATURE_SPOOFING=restricted" \
-e "OTA_URL=https://ota.ecloud.global/api" \
-e "REPO=https://gitlab.e.foundation/e/os/android.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:latest
```
>>>

---
>>>
The device code can be found on [/e/ wiki](https://gitlab.e.foundation/e/wiki/en/wikis/devices-list), [LineageOS wiki](https://wiki.lineageos.org/devices/) or with the following command:
```shell
$ adb shell getprop ro.product.device
```
>>>

## 5. Get your image!

When your build is finished, please find your images inside `/e/zips/<my-device>` folder. To install, please refer to our [wiki](https://gitlab.e.foundation/e/wiki/en/wikis/devices-list). 

If you need help, please join us on our [community support Telegram channel](https://t.me/joinchat/Fzzi3kUbP-AcoQz3zYHl5A).

To find more information about our docker image and its environment variables [here](https://gitlab.e.foundation/e/os/docker-lineage-cicd).

To report an issue about a build, please refer to [wiki issues documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues)
