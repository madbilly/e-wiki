# /e/ wiki

## Create /e/ test account

In order to test the /e/ identity features in the /e/ operating system (single identity for email, drive, calendar, notes and tasks), you have to use an /e/ account.

If you want to get a test account, please send an email to contact@e.email with “test account” in subject.
It will be processed automatically so check for typos.

We have opened a first batch of 100 test accounts, and a second batch of 200 test accounts.

<i>"Create /e/ test account" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
