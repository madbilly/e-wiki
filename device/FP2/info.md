
# /e/ wiki

## **Fairphone** FP2 - *"FP2"*

### Device informations

* Brand: Fairphone
* Model: FP2
* Code: FP2

### Download

Download /e/ for **Fairphone** FP2 - *"FP2"*
* [nightly](https://images.ecloud.global/nightly/FP2/)

### Install

[Install /e/ on **Fairphone** FP2 - *"FP2"*](device/FP2/install)

### Build

[Build /e/ for **Fairphone** FP2 - *"FP2"*](device/FP2/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Fairphone** FP2 - *"FP2"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
