
# /e/ wiki

## Install /e/ on **Fairphone** FP2 - *"FP2"*

>>>
**Very important: please read the following carefully before proceeding!**

Installing a new operating system on a mobile device can potentially:
1. lead to all data destruction on the device
1. make it an unrecoverable brick.

So please only flash your device if you know what you are doing and are OK with taking the associated risk.

The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and/or /e/ services.
>>>

### Requirements

1. *Optionnal*: It is recommended that you have an /e/ account (like: john.doe@e.email) if you want to benefit from /e/ account integration for all online services such as: email, drive, calendar, notes, tasks. In order to get a test /e/ account, please read instructions [here](create-e-test-account).
1. Make sure your computer has working `adb` and `fastboot`. Setup instructions can be found [here](https://wiki.lineageos.org/adb_fastboot_guide.html).
1. Enable USB debugging on your device.
1. Please read through the instructions at least once completely before actually following them to avoid any problems because you missed something!

### Installing a custom recovery

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/FP2).
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing

    ```shell
    fastboot devices
    ```
1. Flash recovery onto your device

    ```shell
    fastboot flash recovery twrp-x.x.x-x-FP2.img
    ```
1. Now reboot into recovery to verify the installation

### Installing /e/ from custom recovery

1. Download the /e/ install package that you’d like to install from [here](https://images.ecloud.global/nightly/FP2/).
1. If you aren’t already in recovery mode, reboot into recovery mode:

    ```shell
    adb reboot recovery
    ```
1. *Optional* Tap the Backup button to create a backup. Make sure the backup is created in the external sdcard or copy it onto your computer as the internal storage will be formatted.
1. Go back to return to main menu, then tap Wipe.
1. Now tap Format Data and continue with the formatting process. This will remove encryption as well as delete all files stored on the internal storage.
1. Return to the previous menu and tap Advanced Wipe.
1. Select the Cache and System partitions to be wiped and then Swipe to Wipe.
1. Place the /e/ .zip package on the root of /sdcard:

    ```
    adb push filename.zip /sdcard/
    ```
1. Go back to return to main menu, then tap Install.
1. Navigate to /sdcard, and select the /e/ .zip package.
1. Follow the on-screen prompts to install the package.
1. Once installation has finished, return to the main menu, tap Reboot, and then System

<i>This documentation "Install /e/ on **Fairphone** FP2 - *"FP2"*" is a derivative of ["install LineageOS on FP2"](https://wiki.lineageos.org/devices/FP2/install) by The LineageOS Project, used under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) . "Install /e/ on **Fairphone** FP2 - *"FP2"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>

