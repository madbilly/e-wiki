
# /e/ wiki

## **Samsung** Galaxy A5 (2017) - *"a5y17lte"*

### Device informations

* Brand: Samsung
* Model: Galaxy A5 (2017)
* Code: a5y17lte

### Download

Download /e/ for **Samsung** Galaxy A5 (2017) - *"a5y17lte"*
* [nightly](https://images.ecloud.global/nightly/a5y17lte/)

### Install

[Install /e/ on **Samsung** Galaxy A5 (2017) - *"a5y17lte"*](device/a5y17lte/install)

### Build

[Build /e/ for **Samsung** Galaxy A5 (2017) - *"a5y17lte"*](device/a5y17lte/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy A5 (2017) - *"a5y17lte"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
