
# /e/ wiki

## **Motorola** Moto G4 - *"athene"*

### Device informations

* Brand: Motorola
* Model: Moto G4
* Code: athene

### Download

Download /e/ for **Motorola** Moto G4 - *"athene"*
* [nightly](https://images.ecloud.global/nightly/athene/)

### Install

[Install /e/ on **Motorola** Moto G4 - *"athene"*](device/athene/install)

### Build

[Build /e/ for **Motorola** Moto G4 - *"athene"*](device/athene/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto G4 - *"athene"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
