
# /e/ wiki

## **OnePlus** OnePlus One - *"bacon"*

### Device informations

* Brand: OnePlus
* Model: OnePlus One
* Code: bacon

### Download

Download /e/ for **OnePlus** OnePlus One - *"bacon"*
* [nightly](https://images.ecloud.global/nightly/bacon/)

### Install

[Install /e/ on **OnePlus** OnePlus One - *"bacon"*](device/bacon/install)

### Build

[Build /e/ for **OnePlus** OnePlus One - *"bacon"*](device/bacon/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**OnePlus** OnePlus One - *"bacon"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
