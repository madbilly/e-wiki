
# /e/ wiki

## **Xiaomi** Mi 5s - *"capricorn"*

### Device informations

* Brand: Xiaomi
* Model: Mi 5s
* Code: capricorn

### Download

Download /e/ for **Xiaomi** Mi 5s - *"capricorn"*
* [nightly](https://images.ecloud.global/nightly/capricorn/)

### Install

[Install /e/ on **Xiaomi** Mi 5s - *"capricorn"*](device/capricorn/install)

### Build

[Build /e/ for **Xiaomi** Mi 5s - *"capricorn"*](device/capricorn/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Xiaomi** Mi 5s - *"capricorn"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
