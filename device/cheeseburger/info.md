
# /e/ wiki

## **OnePlus** OnePlus 5 - *"cheeseburger"*

### Device informations

* Brand: OnePlus
* Model: OnePlus 5
* Code: cheeseburger

### Download

Download /e/ for **OnePlus** OnePlus 5 - *"cheeseburger"*
* [nightly](https://images.ecloud.global/nightly/cheeseburger/)

### Install

[Install /e/ on **OnePlus** OnePlus 5 - *"cheeseburger"*](device/cheeseburger/install)

### Build

[Build /e/ for **OnePlus** OnePlus 5 - *"cheeseburger"*](device/cheeseburger/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**OnePlus** OnePlus 5 - *"cheeseburger"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
