
# /e/ wiki

## **Motorola** Moto E - *"condor"*

### Device informations

* Brand: Motorola
* Model: Moto E
* Code: condor

### Download

Download /e/ for **Motorola** Moto E - *"condor"*
* [nightly](https://images.ecloud.global/nightly/condor/)

### Install

[Install /e/ on **Motorola** Moto E - *"condor"*](device/condor/install)

### Build

[Build /e/ for **Motorola** Moto E - *"condor"*](device/condor/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto E - *"condor"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
