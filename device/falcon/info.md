
# /e/ wiki

## **Motorola** Moto G - *"falcon"*

### Device informations

* Brand: Motorola
* Model: Moto G
* Code: falcon

### Download

Download /e/ for **Motorola** Moto G - *"falcon"*
* [nightly](https://images.ecloud.global/nightly/falcon/)

### Install

[Install /e/ on **Motorola** Moto G - *"falcon"*](device/falcon/install)

### Build

[Build /e/ for **Motorola** Moto G - *"falcon"*](device/falcon/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto G - *"falcon"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
