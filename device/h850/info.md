
# /e/ wiki

## **LG** G5 (International) - *"h850"*

### Device informations

* Brand: LG
* Model: G5 (International)
* Code: h850

### Download

Download /e/ for **LG** G5 (International) - *"h850"*
* [nightly](https://images.ecloud.global/nightly/h850/)

### Install

[Install /e/ on **LG** G5 (International) - *"h850"*](device/h850/install)

### Build

[Build /e/ for **LG** G5 (International) - *"h850"*](device/h850/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**LG** G5 (International) - *"h850"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
