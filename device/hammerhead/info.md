
# /e/ wiki

## **Google** Nexus 5 - *"hammerhead"*

### Device informations

* Brand: Google
* Model: Nexus 5
* Code: hammerhead

### Download

Download /e/ for **Google** Nexus 5 - *"hammerhead"*
* [nightly](https://images.ecloud.global/nightly/hammerhead/)

### Install

[Install /e/ on **Google** Nexus 5 - *"hammerhead"*](device/hammerhead/install)

### Build

[Build /e/ for **Google** Nexus 5 - *"hammerhead"*](device/hammerhead/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Google** Nexus 5 - *"hammerhead"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
