
# /e/ wiki

## **Samsung** Galaxy s7 - *"herolte"*

### Device informations

* Brand: Samsung
* Model: Galaxy s7
* Code: herolte

### Download

Download /e/ for **Samsung** Galaxy s7 - *"herolte"*
* [nightly](https://images.ecloud.global/nightly/herolte/)

### Install

[Install /e/ on **Samsung** Galaxy s7 - *"herolte"*](device/herolte/install)

### Build

[Build /e/ for **Samsung** Galaxy s7 - *"herolte"*](device/herolte/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy s7 - *"herolte"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
