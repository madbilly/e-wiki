
# /e/ wiki

## **HTC** One A9 (International GSM) - *"hiaeuhl"*

### Device informations

* Brand: HTC
* Model: One A9
* Code: hiaeuhl

### Download

Download /e/ for **HTC** One A9 (International GSM) - *"hiaeuhl"*
* [nightly](https://images.ecloud.global/nightly/hiaeuhl/)

### Install

[Install /e/ on **HTC** One A9 (International GSM) - *"hiaeuhl"*](device/hiaeuhl/install)

### Build

[Build /e/ for **HTC** One A9 (International GSM) - *"hiaeuhl"*](device/hiaeuhl/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**HTC** One A9 (International GSM) - *"hiaeuhl"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
