
# /e/ wiki

## **Samsung** Galaxy S III (International) - *"i9300"*

### Device informations

* Brand: Samsung
* Model: Galaxy S III (International)
* Code: i9300

### Download

Download /e/ for **Samsung** Galaxy S III (International) - *"i9300"*
* [nightly](https://images.ecloud.global/nightly/i9300/)

### Install

[Install /e/ on **Samsung** Galaxy S III (International) - *"i9300"*](device/i9300/install)

### Build

[Build /e/ for **Samsung** Galaxy S III (International) - *"i9300"*](device/i9300/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy S III (International) - *"i9300"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
