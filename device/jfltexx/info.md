
# /e/ wiki

## **Samsung** Galaxy S4 - *"jfltexx"*

### Device informations

* Brand: Samsung
* Model: Galaxy S4
* Code: jfltexx

### Download

Download /e/ for **Samsung** Galaxy S4 - *"jfltexx"*
* [nightly](https://images.ecloud.global/nightly/jfltexx/)

### Install

[Install /e/ on **Samsung** Galaxy S4 - *"jfltexx"*](device/jfltexx/install)

### Build

[Build /e/ for **Samsung** Galaxy S4 - *"jfltexx"*](device/jfltexx/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy S4 - *"jfltexx"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
