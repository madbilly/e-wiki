
# /e/ wiki

## **Xiaomi** Redmi Note 3 - *"kenzo"*

### Device informations

* Brand: Xiaomi
* Model: Redmi Note 3
* Code: kenzo

### Download

Download /e/ for **Xiaomi** Redmi Note 3 - *"kenzo"*
* [nightly](https://images.ecloud.global/nightly/kenzo/)

### Install

[Install /e/ on **Xiaomi** Redmi Note 3 - *"kenzo"*](device/kenzo/install)

### Build

[Build /e/ for **Xiaomi** Redmi Note 3 - *"kenzo"*](device/kenzo/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Xiaomi** Redmi Note 3 - *"kenzo"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
