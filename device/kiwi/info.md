
# /e/ wiki

## **Huawei** Honor 5X - *"kiwi"*

### Device informations

* Brand: Huawei
* Model: Honor 5X
* Code: kiwi

### Download

Download /e/ for **Huawei** Honor 5X - *"kiwi"*
* [nightly](https://images.ecloud.global/nightly/kiwi/)

### Install

[Install /e/ on **Huawei** Honor 5X - *"kiwi"*](device/kiwi/install)

### Build

[Build /e/ for **Huawei** Honor 5X - *"kiwi"*](device/kiwi/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Huawei** Honor 5X - *"kiwi"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
