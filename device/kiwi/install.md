
# /e/ wiki

## Install /e/ on **Huawei** Honor 5X - *"kiwi"*

>>>
**Very important: please read the following carefully before proceeding!**

Installing a new operating system on a mobile device can potentially:
1. lead to all data destruction on the device
1. make it an unrecoverable brick.

So please only flash your device if you know what you are doing and are OK with taking the associated risk.

The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and/or /e/ services.
>>>

### Requirements

1. *Optionnal*: It is recommended that you have an /e/ account (like: john.doe@e.email) if you want to benefit from /e/ account integration for all online services such as: email, drive, calendar, notes, tasks. In order to get a test /e/ account, please read instructions [here](create-e-test-account).
1. Make sure your computer has working `adb` and `fastboot`. Setup instructions can be found [here](https://wiki.lineageos.org/adb_fastboot_guide.html).
1. Enable USB debugging on your device.
1. Please read through the instructions at least once completely before actually following them to avoid any problems because you missed something!

### Unlocking the bootloader

>>>
Warning: Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or a online drive.
>>>

1. Enable OEM unlock in the Developer options under device Settings
1. Visit [Huawei’s official unlocking website](https://hwid1.vmall.com/CAS/portal/loginAuth.html?service=http%3A%2F%2Fwww.emui.com%2Fplugin.php%3Fid%3Dhwlogin%3Avalidate%26ru%3Dhttp%253A%252F%252Fwww.emui.com%252Fplugin.php%253Fid%253Dunlock%2526mod%253Dunlock%2526action%253Dapply&loginChannel=22000000&reqClientType=22&deviceID=&lang=en-us&InviterUserID=&Inviter=&isFromPC=1&loginUrl=https%3A%2F%2Fhwid1.vmall.com%2Foauth2%2Fportal%2Fcloud_login.jsp&adUrl=http%3A%2F%2Fwww.emui.com%2Fsource%2Fplugin%2Fhwlogin%2Flogin.php&viewType=1&isDialog=0&themeName=cloudTheme), you’ll be asked to login first.
1. Follow the instructions and get your unlock password.
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:

    ```shell
    fastboot devices
    ```
1. Now type the following command to unlock the bootloader:

    ```shell
    fastboot oem unlock ****************
    ```
    Replace ****** with the 16-digit unique unlock password that was obtained in step 2.
1. Wait for the bootloader unlocking process to complete. Once finished, your device will reboot to system and will be restored to factory settings. If an incorrect password is entered, a message will be displayed and your device will also reboot to system.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.
1. To check the bootloader lock status, reboot the device to fastboot mode (repeating steps 3 and 4) and type:

    ```shell
    fastboot oem get-bootinfo
    ```
    The message `Bootloader Lock State: LOCKED` indicates that the bootloader is still locked. Perform the unlocking procedure again and check that the password was entered correctly. On the other hand, the message `Bootloader Lock State: UNLOCKED` indicates that the bootloader has been unlocked. You can now install third-party firmware.



### Installing a custom recovery

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/kiwi).
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing

    ```shell
    fastboot devices
    ```
1. Flash recovery onto your device

    ```shell
    fastboot flash recovery twrp-x.x.x-x-kiwi.img
    ```
1. Now reboot into recovery to verify the installation

### Installing /e/ from custom recovery

1. Download the /e/ install package that you’d like to install from [here](https://images.ecloud.global/nightly/kiwi/).
1. If you aren’t already in recovery mode, reboot into recovery mode:

    ```shell
    adb reboot recovery
    ```
1. *Optional* Tap the Backup button to create a backup. Make sure the backup is created in the external sdcard or copy it onto your computer as the internal storage will be formatted.
1. Go back to return to main menu, then tap Wipe.
1. Now tap Format Data and continue with the formatting process. This will remove encryption as well as delete all files stored on the internal storage.
1. Return to the previous menu and tap Advanced Wipe.
1. Select the Cache and System partitions to be wiped and then Swipe to Wipe.
1. Place the /e/ .zip package on the root of /sdcard:

    ```
    adb push filename.zip /sdcard/
    ```
1. Go back to return to main menu, then tap Install.
1. Navigate to /sdcard, and select the /e/ .zip package.
1. Follow the on-screen prompts to install the package.
1. Once installation has finished, return to the main menu, tap Reboot, and then System

<i>This documentation "Install /e/ on **Huawei** Honor 5X - *"kiwi"*" is a derivative of ["install LineageOS on kiwi"](https://wiki.lineageos.org/devices/kiwi/install) by The LineageOS Project, used under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) . "Install /e/ on **Huawei** Honor 5X - *"kiwi"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
