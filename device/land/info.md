
# /e/ wiki

## **Xiaomi** Redmi 3S/3X - *"land"*

### Device informations

* Brand: Xiaomi
* Model: Redmi 3S/3X
* Code: land

### Download

Download /e/ for **Xiaomi** Redmi 3S/3X - *"land"*
* [nightly](https://images.ecloud.global/nightly/land/)

### Install

[Install /e/ on **Xiaomi** Redmi 3S/3X - *"land"*](device/land/install)

### Build

[Build /e/ for **Xiaomi** Redmi 3S/3X - *"land"*](device/land/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Xiaomi** Redmi 3S/3X - *"land"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
