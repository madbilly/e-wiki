
# /e/ wiki

## **HTC** One (M8) - *"m8"*

### Device informations

* Brand: HTC
* Model: One (M8)
* Code: m8

### Download

Download /e/ for **HTC** One (M8) - *"m8"*
* [nightly](https://images.ecloud.global/nightly/m8/)

### Install

[Install /e/ on **HTC** One (M8) - *"m8"*](device/m8/install)

### Build

[Build /e/ for **HTC** One (M8) - *"m8"*](device/m8/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**HTC** One (M8) - *"m8"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
