
# /e/ wiki

## **Google** Nexus 4 - *"mako"*

### Device informations

* Brand: Google
* Model: Nexus 4
* Code: mako

### Download

Download /e/ for **Google** Nexus 4 - *"mako"*
* [nightly](https://images.ecloud.global/nightly/mako/)

### Install

[Install /e/ on **Google** Nexus 4 - *"mako"*](device/mako/install)

### Build

[Build /e/ for **Google** Nexus 4 - *"mako"*](device/mako/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Google** Nexus 4 - *"mako"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
