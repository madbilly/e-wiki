
# /e/ wiki

## Essential Phone PH1  - *"mata"*

### Device informations

* Brand: Essential
* Model: PH1
* Code: mata

### Download

Download /e/ for Essential Phone PH1 - *"mata"*
* [nightly](https://images.ecloud.global/nightly/mata/)

### Install

[Install /e/ on Essential Phone PH1 - *"mata"*](device/mata/install)

### Build

[Build /e/ for Essential Phone PH1 - *"mata"*](device/mata/build)

### What's not working

* system updater sometimes has weird behaviour (need to restart it to be able to install)
 
Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"Essential Phone PH1 - *"mata"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
