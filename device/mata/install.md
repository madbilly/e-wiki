
# /e/ wiki

## Install /e/ on Essential PH1  - *"mata"*

>>>
**Very important: please read the following carefully before proceeding!**

Installing a new operating system on a mobile device can potentially:
1. lead to all data destruction on the device
1. make it an unrecoverable brick.

So please only flash your device if you know what you are doing and are OK with taking the associated risk.

The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and/or /e/ services.
>>>

### Notes

Some useful additional information can be found at:
1. https://forum.xda-developers.com/essential-phone/development/rom-lineageos-14-1-essential-ph-1-mata-t3703759
1. https://forum.xda-developers.com/essential-phone/development/stock-7-1-1-nmj20d-t3701681
1. https://mata.readthedocs.io/en/latest/

### Requirements

1. *Optionnal*: It is recommended that you have an /e/ account (like: john.doe@e.email) if you want to benefit from /e/ account integration for all online services such as: email, drive, calendar, notes, tasks. In order to get a test /e/ account, please read instructions [here](create-e-test-account).
1. Make sure your computer has working `adb` and `fastboot`. Setup instructions can be found [here](https://wiki.lineageos.org/adb_fastboot_guide.html).
1. Enable USB debugging on your device.
1. In can of issue, please use another USB cable than the one provided by Essential
1. Please read through the instructions at least once completely before actually following them to avoid any problems because you missed something!

### Unlocking the bootloader

1. Unlock bootloader

    ```shell
    fastboot flashing unlock
    ```

1. Unlock critical partition

    ```shell
    fastboot flashing unlock_critical
    ```

### Flashing firmwares
 
1. Download the [NMJ88C archive](https://mega.nz/#!1CgF3SLa!CHLLA0HvtDtzzDnOl3CbJ1EUnhR6vFb4NA9IG6au3to) and unzip it

1. execute the script:

    On Linux or MacOS X:

    ```shell
    ./flash-all.sh
    ```

    On Windows:

    ```shell
    ./flash-all.bat
    ```

(it takes a while to complete)

### Installing a custom recovery: TWRP for Mata

1. Download [TWRP for Mata](https://gg.gg/9ubmn)
1. Connect your device to your PC via USB.
1. Flash TWRP to the boot partition:

    ```shell
    fastboot -w
    fastboot flash boot twrp-mata_11.img
    ```
1. Stay in bootloader mode, and use the volume down to select "Recovery mode". Then  hit the power button: this should boot you in to recovery mode (TWRP). If it's asking for a password, enter your previous lockscreen password or pin, this will decrypt your data partition. Note: don't be surprised that TWRP will be erased at next boot. Therefore, you will have to reinstall it if you want to install another ROM on your device.

### Installing /e/ from custom recovery

1. Download the /e/ install package that you’d like to install from [here](https://images.ecloud.global/nightly/mata/).
1. In recovery mode, put your device in sideload mode:

    ```shell
    adb shell twrp sideload
    ```

1. Flash the /e/ .zip package:

    ```
    adb sideload </e zip file>
    ```

1. Once installation has finished, you can reboot

<i>"Install /e/ on Essential PH1 - *"mata"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>

