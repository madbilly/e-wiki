
# /e/ wiki

## **Xiaomi** Redmi Note 4 - *"mido"*

### Device informations

* Brand: Xiaomi
* Model: Redmi Note 4
* Code: mido

### Download

Download /e/ for **Xiaomi** Redmi Note 4 - *"mido"*
* [nightly](https://images.ecloud.global/nightly/mido/)

### Install

[Install /e/ on **Xiaomi** Redmi Note 4 - *"mido"*](device/mido/install)

### Build

[Build /e/ for **Xiaomi** Redmi Note 4 - *"mido"*](device/mido/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Xiaomi** Redmi Note 4 - *"mido"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
