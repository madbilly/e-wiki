
# /e/ wiki

## **Xiaomi** Mi 5s Plus - *"natrium"*

### Device informations

* Brand: Xiaomi
* Model: Mi 5s Plus
* Code: natrium

### Download

Download /e/ for **Xiaomi** Mi 5s Plus - *"natrium"*
* [nightly](https://images.ecloud.global/nightly/natrium/)

### Install

[Install /e/ on **Xiaomi** Mi 5s Plus - *"natrium"*](device/natrium/install)

### Build

[Build /e/ for **Xiaomi** Mi 5s Plus - *"natrium"*](device/natrium/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Xiaomi** Mi 5s Plus - *"natrium"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
