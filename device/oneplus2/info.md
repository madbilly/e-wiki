
# /e/ wiki

## **OnePlus** OnePlus 2 - *"oneplus2"*

### Device informations

* Brand: OnePlus
* Model: OnePlus 2
* Code: oneplus2

### Download

Download /e/ for **OnePlus** OnePlus 2 - *"oneplus2"*
* [nightly](https://images.ecloud.global/nightly/oneplus2/)

### Install

[Install /e/ on **OnePlus** OnePlus 2 - *"oneplus2"*](device/oneplus2/install)

### Build

[Build /e/ for **OnePlus** OnePlus 2 - *"oneplus2"*](device/oneplus2/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**OnePlus** OnePlus 2 - *"oneplus2"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
