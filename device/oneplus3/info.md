
# /e/ wiki

## **OnePlus** OnePlus 3/3T - *"oneplus3"*

### Device informations

* Brand: OnePlus
* Model: OnePlus 3/3T
* Code: oneplus3

### Download

Download /e/ for **OnePlus** OnePlus 3/3T - *"oneplus3"*
* [nightly](https://images.ecloud.global/nightly/oneplus3/)

### Install

[Install /e/ on **OnePlus** OnePlus 3/3T - *"oneplus3"*](device/oneplus3/install)

### Build

[Build /e/ for **OnePlus** OnePlus 3/3T - *"oneplus3"*](device/oneplus3/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**OnePlus** OnePlus 3/3T - *"oneplus3"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
