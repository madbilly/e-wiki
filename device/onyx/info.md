
# /e/ wiki

## **OnePlus** OnePlus X - *"onyx"*

### Device informations

* Brand: OnePlus
* Model: OnePlus X
* Code: onyx

### Download

Download /e/ for **OnePlus** OnePlus X - *"onyx"*
* [nightly](https://images.ecloud.global/nightly/onyx/)

### Install

[Install /e/ on **OnePlus** OnePlus X - *"onyx"*](device/onyx/install)

### Build

[Build /e/ for **OnePlus** OnePlus X - *"onyx"*](device/onyx/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**OnePlus** OnePlus X - *"onyx"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
