
# /e/ wiki

## **Motorola** Moto G 2015 - *"osprey"*

### Device informations

* Brand: Motorola
* Model: Moto G 2015
* Code: osprey

### Download

Download /e/ for **Motorola** Moto G 2015 - *"osprey"*
* [nightly](https://images.ecloud.global/nightly/osprey/)

### Install

[Install /e/ on **Motorola** Moto G 2015 - *"osprey"*](device/osprey/install)

### Build

[Build /e/ for **Motorola** Moto G 2015 - *"osprey"*](device/osprey/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto G 2015 - *"osprey"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
