
# /e/ wiki

## **Motorola** Moto G 4G - *"peregrine"*

### Device informations

* Brand: Motorola
* Model: Moto G 4G
* Code: peregrine

### Download

Download /e/ for **Motorola** Moto G 4G - *"peregrine"*
* [nightly](https://images.ecloud.global/nightly/peregrine/)

### Install

[Install /e/ on **Motorola** Moto G 4G - *"peregrine"*](device/peregrine/install)

### Build

[Build /e/ for **Motorola** Moto G 4G - *"peregrine"*](device/peregrine/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto G 4G - *"peregrine"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
