
# /e/ wiki

## **LeEco** Le 2 - *"s2"*

### Device informations

* Brand: LeEco
* Model: Le 2
* Code: s2

### Download

Download /e/ for **LeEco** Le 2 - *"s2"*
* [nightly](https://images.ecloud.global/nightly/s2/)

### Install

[Install /e/ on **LeEco** Le 2 - *"s2"*](device/s2/install)

### Build

[Build /e/ for **LeEco** Le 2 - *"s2"*](device/s2/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**LeEco** Le 2 - *"s2"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
