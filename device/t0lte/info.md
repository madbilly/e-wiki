
# /e/ wiki

## **Samsung** Galaxy Note 2 (LTE) - *"t0lte"*

### Device informations

* Brand: Samsung
* Model: Galaxy Note 2 (LTE)
* Code: t0lte

### Download

Download /e/ for **Samsung** Galaxy Note 2 (LTE) - *"t0lte"*
* [nightly](https://images.ecloud.global/nightly/t0lte/)

### Install

[Install /e/ on **Samsung** Galaxy Note 2 (LTE) - *"t0lte"*](device/t0lte/install)

### Build

[Build /e/ for **Samsung** Galaxy Note 2 (LTE) - *"t0lte"*](device/t0lte/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy Note 2 (LTE) - *"t0lte"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
