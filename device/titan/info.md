
# /e/ wiki

## **Motorola** Moto G 2014 - *"titan"*

### Device informations

* Brand: Motorola
* Model: Moto G 2014
* Code: titan

### Download

Download /e/ for **Motorola** Moto G 2014 - *"titan"*
* [nightly](https://images.ecloud.global/nightly/titan/)

### Install

[Install /e/ on **Motorola** Moto G 2014 - *"titan"*](device/titan/install)

### Build

[Build /e/ for **Motorola** Moto G 2014 - *"titan"*](device/titan/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Motorola** Moto G 2014 - *"titan"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
