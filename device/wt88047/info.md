
# /e/ wiki

## **Wingtech** Redmi 2 - *"wt88047"*

### Device informations

* Brand: Wingtech
* Model: Redmi 2
* Code: wt88047

### Download

Download /e/ for **Wingtech** Redmi 2 - *"wt88047"*
* [nightly](https://images.ecloud.global/nightly/wt88047/)

### Install

[Install /e/ on **Wingtech** Redmi 2 - *"wt88047"*](device/wt88047/install)

### Build

[Build /e/ for **Wingtech** Redmi 2 - *"wt88047"*](device/wt88047/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Wingtech** Redmi 2 - *"wt88047"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
