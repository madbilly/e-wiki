
# /e/ wiki

## **Samsung** Galaxy s6 - *"zerofltexx"*

### Device informations

* Brand: Samsung
* Model: Galaxy s6
* Code: zerofltexx

### Download

Download /e/ for **Samsung** Galaxy s6 - *"zerofltexx"*
* [nightly](https://images.ecloud.global/nightly/zerofltexx/)

### Install

[Install /e/ on **Samsung** Galaxy s6 - *"zerofltexx"*](device/zerofltexx/install)

### Build

[Build /e/ for **Samsung** Galaxy s6 - *"zerofltexx"*](device/zerofltexx/build)

### What's not working

No known bugs

Have you found a bug? Please report [here](https://gitlab.e.foundation/e/management/issues/new) ([documentation](https://gitlab.e.foundation/e/wiki/en/wikis/issues))

<i>"**Samsung** Galaxy s6 - *"zerofltexx"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
