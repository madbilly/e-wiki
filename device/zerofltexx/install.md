
# /e/ wiki

## Install /e/ on **Samsung** Galaxy s6 - *"zerofltexx"*

>>>
**Very important: please read the following carefully before proceeding!**

Installing a new operating system on a mobile device can potentially:
1. lead to all data destruction on the device
1. make it an unrecoverable brick.

So please only flash your device if you know what you are doing and are OK with taking the associated risk.

The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and/or /e/ services.
>>>

### Requirements

1. *Optionnal*: It is recommended that you have an /e/ account (like: john.doe@e.email) if you want to benefit from /e/ account integration for all online services such as: email, drive, calendar, notes, tasks. In order to get a test /e/ account, please read instructions [here](create-e-test-account).
1. Make sure your computer has working `adb` and `fastboot`. Setup instructions can be found [here](https://wiki.lineageos.org/adb_fastboot_guide.html).
1. Enable USB debugging on your device.
1. Please read through the instructions at least once completely before actually following them to avoid any problems because you missed something!

### Preparing for installation

Samsung devices come with a unique boot mode called “Download mode”, which is very similar to “Fastboot mode” on some devices with unlocked bootloaders. Heimdall is a cross-platform, open-source tool for interfacing with Download mode on Samsung devices. The preferred method of installing a custom recovery is through this boot mode – rooting the stock firmware is neither necessary nor required.

1. Download and install the [Heimdall suite](http://glassechidna.com.au/heimdall/#downloads):
    * Windows: Extract the Heimdall suite and take note of the directory containing `heimdall.exe`. You can verify Heimdall is working by opening a command prompt in that directory and typing `heimdall version`. If you receive an error, make sure you have the [Microsoft Visual C++ 2012 Redistributable Package (x86)](https://www.microsoft.com/en-us/download/details.aspx?id=30679) installed on your computer.
    * Linux: Pick the appropriate package to install for your distribution. The `-frontend` packages aren’t needed for this guide. After installation, verify Heimdall is installed by running `heimdall version` in the terminal.
    * macOS: Install the `dmg` package. After installation, Heimdall should be available from the terminal - type `heimdall version` to double-check.
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
    * With the device powered off, hold **Volume Down** + **Home** + **Power**.
    
    Accept the disclaimer, then insert the USB cable into the device.
1. Windows only: install the drivers. A more complete set of instructions can be found in the ZAdiag user guide.
    1. Run `zadiag.exe` from the Drivers folder of the Heimdall suite.
    1. Choose Options » List all devices from the menu.
    1. Select Samsung USB Composite Device or MSM8x60 or Gadget Serial or Device Name from the drop down menu. (If nothing relevant appears, try uninstalling any Samsung related Windows software, like Samsung Windows drivers and/or Kies).
    1. Click Replace Driver (having to select Install Driver from the drop down list built into the 
    1. If you are prompted with a warning that the installer is unable to verify the publisher of the driver, select Install this driver anyway. You may receive two more prompts about security. Select the options that allow you to carry on.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    heimdall print-pit
    ```
1. If the device reboots, Heimdall is installed and working properly.

### Installing a custom recovery

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/zeroflte).
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
    * With the device powered off, hold **Volume Down** + **Home** + **Power**.
    
    Accept the disclaimer, then insert the USB cable into the device.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window in the directory the recovery image is located, and type: 
    ```shell
    heimdall flash --RECOVERY twrp-x.x.x-x-zerofltexx.img --no-reboot
    ```
1. A blue transfer bar will appear on the device showing the recovery being transferred.
1. Unplug the USB cable from your device.
1. Manually reboot into recovery:
    * With the device powered off, hold **Volume Down** + **Home** + **Power**.
    
    > Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).

### Installing /e/ from custom recovery

1. Download the /e/ install package that you’d like to install from [here](https://images.ecloud.global/nightly/zerofltexx/).
1. If you aren’t already in recovery mode, reboot into recovery mode:

    ```shell
    adb reboot recovery
    ```
1. *Optional* Tap the Backup button to create a backup. Make sure the backup is created in the external sdcard or copy it onto your computer as the internal storage will be formatted.
1. Go back to return to main menu, then tap Wipe.
1. Now tap Format Data and continue with the formatting process. This will remove encryption as well as delete all files stored on the internal storage.
1. Return to the previous menu and tap Advanced Wipe.
1. Select the Cache and System partitions to be wiped and then Swipe to Wipe.
1. Place the /e/ .zip package on the root of /sdcard:

    ```
    adb push filename.zip /sdcard/
    ```
1. Go back to return to main menu, then tap Install.
1. Navigate to /sdcard, and select the /e/ .zip package.
1. Follow the on-screen prompts to install the package.
1. Once installation has finished, return to the main menu, tap Reboot, and then System

<i>This documentation "Install /e/ on **Samsung** Galaxy s6 - *"zerofltexx"*" is a derivative of ["install LineageOS on zerofltexx"](https://wiki.lineageos.org/devices/zerofltexx/install) by The LineageOS Project, used under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) . "Install /e/ on **Samsung** Galaxy s6 - *"zerofltexx"*" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>

