# /e/ installation wiki

## Introduction

Please:
* choose a device from the list below
* proceed to installation
* [find community support here](https://t.me/joinchat/AdMoEA9YzUohzhotLu9IWg)
* [report issues here](issues)
* [report your experience here](testimonies)
* [contribute to the project!](projects-looking-for-contributors)

## Devices list and install

### Essential Phone
* [Essential Phone - *"mata"*](device/mata/info)

### Fairphone
* [FP2 - *"FP2"*](device/FP2/info)

### Google
* [Nexus 4 - *"mako"*](device/mako/info)
* [Nexus 5 - *"hammerhead"*](device/hammerhead/info)

### HTC
* [One A9 (International GSM) - *"hiaeuhl"*](device/hiaeuhl/info)
* [One (M8) - *"m8"*](device/m8/info)

### Huawei
* [Honor 5X - *"kiwi"*](device/kiwi/info)

### LeEco
* [Le 2 - *"s2"*](device/s2/info)

### LG
* [G5 (International) - *"h850"*](device/h850/info)

### Motorola
* [Moto E - *"condor"*](device/condor/info)
* [Moto G - *"falcon"*](device/falcon/info)
* [Moto G 2014 - *"titan"*](device/titan/info)
* [Moto G 2015 - *"osprey"*](device/osprey/info)
* [Moto G 4G - *"peregrine"*](device/peregrine/info)
* [Moto G4 - *"athene"*](device/athene/info)

### OnePlus
* [OnePlus 2 - *"oneplus2"*](device/oneplus2/info)
* [OnePlus 3/3T - *"oneplus3"*](device/oneplus3/info)
* [OnePlus 5 - *"cheeseburger"*](device/cheeseburger/info)
* [OnePlus One - *"bacon"*](device/bacon/info)
* [OnePlus X - *"onyx"*](device/onyx/info)

### Samsung
* [Galaxy A5 (2017) - *"a5y17lte"*](device/a5y17lte/info)
* [Galaxy Note 2 (LTE) - *"t0lte"*](device/t0lte/info)
* [Galaxy s4 - *"jfltexx"*](device/jfltexx/info)
* [Galaxy s6 - *"zerofltexx"*](device/zerofltexx/info)
* [Galaxy s7 - *"herolte"*](device/herolte/info)
* [Galaxy S III (International) - *"i9300"*](device/i9300/info)

### Wingtech
* [Redmi 2 - *"wt88047"*](device/wt88047/info)

### Xiaomi
* [Mi 5s - *"capricorn"*](device/capricorn/info)
* [Mi 5s Plus - *"natrium"*](device/natrium/info)
* [Redmi 3S/3X - *"land"*](device/land/info)
* [Redmi Note 3 - *"kenzo"*](device/kenzo/info)
* [Redmi Note 4 - *"mido"*](device/mido/info)

<i>"Devices list" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018.</i>
