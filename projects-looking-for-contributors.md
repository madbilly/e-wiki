# /e/

## Projects looking for contributors

We have several projects that need contributors:

* port to Oreo/LOS15 core: we have started to port to LOS15. If you want to join the effort, look at the eelo-0.2 branch [in projects](https://gitlab.e.foundation/e/os). ROM maintainers welcome! You can contact us directly as well (contact@e.email)

* [spot](https://gitlab.e.foundation/e/cloud/my-spot): we are running a meta search engine that was forked from SearX. We still need to improve the UI/UX, but more particularly we need to improve its speed. The reason for its slow speed is probably due to sequential sub-calls (to real search engines), instead of parallel calls. The cache feature we have added also probably needs some love, and we could implement other techniques we have in mind. If you know python well, please get in touch about this project (contact@e.email) if you have any questions and/or [commit code suggestions](https://gitlab.e.foundation/e/cloud/my-spot)! 

* [/e/TWRP](https://gitlab.e.foundation//e/tools/etwrp): we'd like to have a customized version of TWRP to make the system update UX smoother. C/Android programmer here! You can [contribute](https://gitlab.e.foundation//e/tools/etwrp) to and/or test what has been started here (contact us at: contact@e.email)

* OpenCamera: some issues have been detected on some devices, probably related to some hardware drivers/firmwares. For instance, Open Camera is randomly crashing on LeEco Le2 and probably other devices, depending on some settings. Also, we'd like to have the best ever open source camera application in term of usability and picture result. If interested in this project please contact us (contact@e.email) and we will start a fork (+ contribute upstream).

* personal assistant: this is going to be a key project for /e/: we need a personal assistant that respects your privacy. The project has not really been started but we have some ideas and a general roadmap. Please contact us about it: contact@e.email 